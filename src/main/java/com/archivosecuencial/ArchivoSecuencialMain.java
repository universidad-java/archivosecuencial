package com.archivosecuencial;


import java.io.File;
import java.time.LocalDateTime;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.archivosecuencial.entity.Registro;
import com.archivosecuencial.entity.Registros;
import com.google.gson.Gson;
public class ArchivoSecuencialMain {

	public static void main(String[] args) {
		
		Scanner menu = new Scanner (System.in);
		
		boolean salir = false;
		int opcion;
		LeerArchivo leer = new LeerArchivo();
		
	while (!salir) {
			
			System.out.println("\n1. Opcion 1: Crear Archivo");
			System.out.println("2. Opcion 2: Leer Archivo");
			System.out.println("3. Opcion 3: Escribir Archivo");
			System.out.println("4. Salir");
			
			try{
			System.out.println("Escoge una opcion: ");
			opcion = menu.nextInt();
			
			switch(opcion) {
				case 1:
					System.out.println("Crearemos un archivo");
					CrearArchivo archivo = new CrearArchivo(); //Creamos una instancia del metodo CrearArchivo
					archivo.crear("Registro.txt");
					break;
					
				case 2:
					System.out.println("Escribiremos en el archivo");
					leer.leer("Registro.txt");
					
					
					break;
						
					
				case 3:
					EscribirArchivo escribir = new EscribirArchivo();
					File archivoescritura = new File("Registro.txt");
					
					
					Registro datos = new Registro();
					EntradaArchivo entrada = new EntradaArchivo();
					datos.setCedula(entrada.entrada());
					datos.setPrimerNombre(entrada.entrada());
					datos.setApellidoPaterno(entrada.entrada());
					datos.setCuenta(entrada.numero());
					datos.setFechaCompra(entrada.entrada());
					datos.setProducto(entrada.entrada());
					
					Registros listadoregistros = new Gson().fromJson(leer.leer("Registro.txt"), Registros.class);
					if (listadoregistros == null) {
						listadoregistros = new Registros();
					}
					listadoregistros.getRegistroslist().add(datos);
					escribir.escribir(new Gson().toJson(listadoregistros), archivoescritura);			
					
					
					break; 
					
				case 4:
					salir = true;
					break;
				default:
					System.out.println("Las opciones son entre 1 y 4");
			}}
			catch(InputMismatchException e) {
				System.out.println("Debes introducir un numero");
				menu.next();
			}	
			
			
			System.out.println("Gracias por utilizar nuestro programa. Att. Solange y Jostin");
		}		
	}

}
