package com.archivosecuencial;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class EscribirArchivo {
	public boolean escribir(String texto, File archivo) {
		try {
			FileWriter writer = new FileWriter(archivo);
			BufferedWriter bufferedWriter = new BufferedWriter(writer);
			bufferedWriter.write(texto);
			bufferedWriter.close();
			return true;
			
		} catch (Exception e) {
			return false;
		}
		
	}
}
