package com.archivosecuencial;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class LeerArchivo {
	
	public String leer (String nombre) {
		
		String line = "";
		try {
			Scanner input = new Scanner(new File(nombre));
			while (input.hasNextLine()) {
                line = line+input.nextLine();
                System.out.println(line);
            }
            input.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		return line;
	}
}
