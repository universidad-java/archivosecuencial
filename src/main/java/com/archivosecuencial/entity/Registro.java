package com.archivosecuencial.entity;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class Registro {

	private int cuenta;
	private String primerNombre;
	private String apellidoPaterno;
	private String cedula;
	private String fechaNacimiento;
	private String saldo;
	private String producto;
	private String fechaCompra;	
}
