package com.archivosecuencial.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Registros {
	List<Registro> registroslist = new ArrayList<Registro>();
}
