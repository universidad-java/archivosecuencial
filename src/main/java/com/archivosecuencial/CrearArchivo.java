/**
 * 
 */
package com.archivosecuencial;

import java.io.File;

/**
 * @author Solange
 *
 */
public class CrearArchivo {

	public File crear(String nombre) { //Creamos un metodo para crear el archivo
		
		try {
			File file = new File (nombre);
			if (!file.exists()) {
				file.createNewFile();
				return file;	
			}else {
				return file;
			}
			
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}
		
}

